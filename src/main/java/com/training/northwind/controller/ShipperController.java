package com.training.northwind.controller;

import com.training.northwind.entities.Shipper;
import com.training.northwind.service.ShipperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/shipper")
public class ShipperController {

    private static final Logger LOG = LoggerFactory.getLogger(ShipperController.class);

    @Autowired
    private ShipperService shipperService;

    @GetMapping
    public List<Shipper> findAll() {
        return shipperService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Shipper> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<Shipper>(shipperService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Shipper> create(@RequestBody Shipper shipper){
        return new ResponseEntity<Shipper>(shipperService.save(shipper), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Shipper> update(@RequestBody Shipper shipper) {
        try {
            return new ResponseEntity<>(shipperService.update(shipper), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + shipper + "]");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            shipperService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
